
function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.tackle= function(target){
		console.log(target) // containes charizard obj

		console.log(`${this.name} tackled ${target.name}`);
		
		console.log(`${target.name}\'s health is now reduced to ${target.health - this.attack}`)
		target.health = target.health - this.attack
	};
	this.faint = function(target){
		if (target.health < 10) {
			console.log(`${target.name} fainted`)
		} else {
			console.log(`${target.name} survived`)
		}
	}
}


let bulbasaur = new Pokemon("Bulbasaur", 5, 50);
let charmander = new Pokemon("Charmander", 8, 40);

console.log(bulbasaur.tackle(charmander));
console.log(bulbasaur.faint(charmander));
console.log(bulbasaur.tackle(charmander));
console.log(bulbasaur.faint(charmander));
console.log(bulbasaur.tackle(charmander));
console.log(bulbasaur.faint(charmander));
console.log(bulbasaur.tackle(charmander));
console.log(bulbasaur.faint(charmander));
console.log(bulbasaur.tackle(charmander));
console.log(bulbasaur.faint(charmander));
console.log(bulbasaur.tackle(charmander));
console.log(bulbasaur.faint(charmander));